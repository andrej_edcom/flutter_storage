import 'package:ariadna/fininfo/repo/fininfo.dart';

class FininfoResponse {
  List<Fininfo> results;

  FininfoResponse({this.results});

  FininfoResponse.fromJson(List<dynamic> json) {

      results = new List<Fininfo>();
      results = json.map((i)=>Fininfo.fromJson(i)).toList();

  }

}