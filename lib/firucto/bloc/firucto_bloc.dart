import 'dart:async';
import 'package:ariadna/base/bloc/bloc.dart';
import 'package:ariadna/firucto/repo/firucto_repository.dart';
import 'package:ariadna/locator.dart';
import 'package:rxdart/rxdart.dart';

class FiructoBloc implements Bloc {

  FiructoRepository fiructoRepository = locator<FiructoRepository>();

  String xfir = "";

  final _fiructoController = BehaviorSubject<String>();

  StreamSink<String> get fiructoSink =>
      _fiructoController.sink;

  Stream<String> get fiructoStream =>
      _fiructoController.stream;

  FiructoBloc() {

    getFiructo();
  }

  Future<String> deviceId() async {
    return fiructoRepository.getDeviceId();
  }

  getFiructo() async {
    String _deviceId = await deviceId();
    xfir = await fiructoRepository.getFir(_deviceId, 'user.fir');
    if(xfir == null) { xfir = ""; }
    fiructoSink.add(xfir);
  }

  void selectFiructo(String fir, String name) {
    if(fir != "") {
      String firname = fir + " - " + name;
      fiructoSink.add(firname);
    }
  }

  void saveFiructo(String fir, String name) async {
    if(fir != "") {
      String firname = fir + " - " + name;
      String _deviceId = await deviceId();
      await fiructoRepository.saveFir(_deviceId, 'user.fir', firname);
    }
  }

  void removeFiructo() async {
    String _deviceId = await deviceId();
    await fiructoRepository.saveFir(_deviceId, 'user.fir', "");
    fiructoSink.add("");
  }

  @override
  void dispose() {
    _fiructoController?.close();
  }
}
