import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ariadna/base/provider/viewstate.dart';
import 'package:ariadna/base/provider/base_view_provider.dart';
import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/base/ui/widgets/error_dialog.dart';
import 'package:ariadna/post/repo/post.dart';
import 'package:ariadna/post/provider/post_model.dart';
import 'package:ariadna/post/ui/shared/app_colors.dart';
import 'package:ariadna/post/ui/shared/text_styles.dart';
import 'package:ariadna/post/ui/shared/ui_helpers.dart';
import 'package:ariadna/post/ui/widgets/postlist_item.dart';

/**
 * by Flutter Architecture - My Provider Implementation Guide
 * https://www.filledstacks.com/post/flutter-architecture-my-provider-implementation-guide/
 * code https://github.com/FilledStacks/flutter-tutorials/010-provider-architecture
 *
 */

class PostsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //return postsScreenWithViewState(context);
    return postsScreenWithApiResponse(context);
  }

  Widget postsScreenWithViewState(BuildContext context) =>
      BaseViewProvider<PostModel>(
        onModelReady: (model) => model.getPosts(1),
        builder: (context, model, child) => Scaffold(
          backgroundColor: backgroundColor,
          body: bodyWithViewState(model),
        ),
      );

  Widget bodyWithViewState(PostModel model) {
    return model.state == ViewState.Busy
        ? Center(child: CircularProgressIndicator())
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              UIHelper.verticalSpaceLarge(),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text(
                  'Welcome User',
                  style: headerStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text('Here are all your posts', style: subHeaderStyle),
              ),
              UIHelper.verticalSpaceSmall(),
              Expanded(child: getPostsUi(model.posts)),
            ],
          );
  }

  Widget postsScreenWithApiResponse(BuildContext context) =>
      BaseViewProvider<PostModel>(
        onModelReady: (model) => model.getApiResponsePosts(1),
        builder: (context, model, child) => Scaffold(
          backgroundColor: backgroundColor,
          body: bodyWithApiResponse(model),
        ),
      );

  Widget bodyWithApiResponse(PostModel model) {

      switch (model.apiResponsePosts.status) {
        case Status.LOADING:
          return Center(child: CircularProgressIndicator());
          break;
        case Status.COMPLETED:
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              UIHelper.verticalSpaceLarge(),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text(
                  'Welcome User',
                  style: headerStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text('Here are all your posts', style: subHeaderStyle),
              ),
              UIHelper.verticalSpaceSmall(),
              Expanded(child: getPostsUi(model.apiResponsePosts.data)),
            ],
          );
          break;
        case Status.ERROR:
          return ErrorDialog(
            errorMessage: model.apiResponsePosts.message,
            onRetryPressed: () => model.getApiResponsePosts(1),
          );
          break;
      }

  }

  Widget getPostsUi(List<Post> posts) => ListView.builder(
      itemCount: posts.length,
      itemBuilder: (context, index) => PostListItem(
            post: posts[index],
            onTap: () {
              //Navigator.pushNamed(context, 'post', arguments: posts[index]);
            },
          ));
}
