import 'package:ariadna/firucto/bloc/firucto_bloc.dart';
import 'package:flutter/material.dart';
import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/base/bloc/bloc_provider.dart';
import 'package:ariadna/base/ui/widgets/error_dialog.dart';
import 'package:ariadna/firucto/bloc/fir_bloc.dart';
import 'package:ariadna/movie/repo/movie_response.dart';
import 'package:ariadna/firucto/repo/fir.dart';
import 'package:ariadna/firucto/ui/widgets/loading_progress_bar.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/firucto/ui/widgets/firlist_item.dart';

class FiructoScreen extends StatefulWidget {
  @override
  _FiructoScreenState createState() => _FiructoScreenState();
}

class _FiructoScreenState extends State<FiructoScreen> {
  FirBloc bloc;

  @override
  void initState() {
    super.initState();

    bloc = locator<FirBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FirBloc>(
      bloc: bloc,
      child: Scaffold(
        appBar:
            AppBar(elevation: 0.0, title: Text('Ariadna'), actions: <Widget>[]),
        body: RefreshIndicator(
          onRefresh: () => bloc.fetchFirList(),
          child: StreamBuilder<ApiResponse<List<Fir>>>(
            stream: bloc.firListStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                switch (snapshot.data.status) {
                  case Status.LOADING:
                    return LoadingProgressBar(
                        loadingMessage: snapshot.data.message);
                    break;
                  case Status.COMPLETED:
                    return getFirsUi(snapshot.data.data);
                    break;
                  case Status.ERROR:
                    return ErrorDialog(
                      errorMessage: snapshot.data.message,
                      onRetryPressed: () => bloc.fetchFirList(),
                    );
                    break;
                }
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget getFirsUi(List<Fir> firs) => ListView.builder(
      itemCount: firs.length,
      itemBuilder: (context, index) => FirListItem(
            fir: firs[index],
            onTap: () {
              print("Andrejko FiructoScreen onTap " + index.toString());

              final fiructoBloc = BlocProvider.of<FiructoBloc>(context);
              fiructoBloc.selectFiructo(firs[index].fir, firs[index].name);
              fiructoBloc.saveFiructo(firs[index].fir, firs[index].name);
              Navigator.of(context).pop();

            },
          ));
}
