import 'dart:convert';
import 'dart:typed_data';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:device_info/device_info.dart';
import 'dart:io';

/*
LocalKeyValuePersistence to SharedPreferences
 */
class AppSharedPreferences {

  String _generateKey(String userId, String key) {
    return '$userId/$key';
  }

  Future<String> saveImage(String userId, String key, Uint8List image) async {
    final base64Image = Base64Encoder().convert(image);
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_generateKey(userId, key), base64Image);
    return _generateKey(userId, key);
  }

  void saveObject(
      String userId, String key, Map<String, dynamic> object) async {
    final prefs = await SharedPreferences.getInstance();
    final string = JsonEncoder().convert(object);

    await prefs.setString(_generateKey(userId, key), string);
  }

  void saveString(String userId, String key, String value, [String jwtid]) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_generateKey(userId, key), value);
  }

  Future<Uint8List> getImage(String userId, String key) async {
    final prefs = await SharedPreferences.getInstance();
    final base64Image = prefs.getString(_generateKey(userId, key));
    if (base64Image != null) return Base64Decoder().convert(base64Image);
    return null;
  }

  Future<Map<String, dynamic>> getObject(String userId, String key) async {
    final prefs = await SharedPreferences.getInstance();
    final objectString = prefs.getString(_generateKey(userId, key));
    if (objectString != null)
      return JsonDecoder().convert(objectString) as Map<String, dynamic>;
    return null;
  }

  Future<String> getString(String userId, String key, [String jwtid]) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_generateKey(userId, key));
  }

  Future<void> removeImage(String userId, String key) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(_generateKey(userId, key));
  }

  Future<void> removeObject(String userId, String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(_generateKey(userId, key));
  }

  Future<void> removeString(String userId, String key, [String jwtid]) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(_generateKey(userId, key));
  }

  Future<String> getDeviceId() async {
    final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    var deviceId = '';

    if (Platform.isAndroid) {
      final AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceId = await androidInfo.androidId;
    }
    if (Platform.isIOS) {
      final IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceId = await iosInfo.identifierForVendor;
    }

    return deviceId;
  }

}
