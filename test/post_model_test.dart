import 'package:flutter_test/flutter_test.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/post/provider/post_model.dart';
import 'package:ariadna/post/repo/post_repository.dart';
import 'package:ariadna/post/repo/post.dart';
import 'package:ariadna/base/api/api_response.dart';

void main() {
  setupLocator();

  var postModel = locator<PostModel>();
  postModel.postRepository = MockPostRepository();

  group('postModelc tests', () {
    test('PostModel test response Status.Completed ', () async {
      await postModel.getApiResponsePosts(1);
      expect(postModel.apiResponsePosts.status, Status.COMPLETED);
    });
    test('PostModel test response Data', () async {
      await postModel.getApiResponsePosts(1);
      expect(postModel.apiResponsePosts.data[0].id, 1);
      expect(postModel.apiResponsePosts.data[0].userId, 1);
      expect(postModel.apiResponsePosts.data[0].title, "title 1");
      expect(postModel.apiResponsePosts.data[0].body, "body 1");
    });
  });

}


class MockPostRepository extends PostRepository {
  @override
  Future<List<Post>> fetchPostList(int userId)  {

    var result = new List<Post>();
    var post = new Post();
    post.userId = 1;
    post.id = 1;
    post.title = "title 1";
    post.body = "body 1";

    result.add(post);
    return Future.value(result);
  }
}