import 'package:ariadna/login/repo/user.dart';

class UserResponse {
  List<User> results;

  UserResponse({this.results});

  UserResponse.fromJson(List<dynamic> json) {

    results = new List<User>();
    results = json.map((i)=>User.fromJson(i)).toList();

  }

}