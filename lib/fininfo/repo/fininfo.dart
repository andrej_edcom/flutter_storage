class Fininfo {
  int userId;
  int id;
  String title;
  String body;
  String cin;
  int tin;
  String vatin;
  String name;
  String street;
  String street_number;
  String postal_code;
  String municipality;

  Fininfo({this.userId, this.id, this.title, this.body, this.cin, this.tin, this.vatin
    , this.name, this.street, this.street_number, this.postal_code, this.municipality});

  Fininfo.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    id = json['id'];
    title = json['title'];
    body = json['body'];
    cin = json['cin'];
    tin = json['tin'];
    vatin = json['vatin'];
    name = json['name'];
    street = json['street'];
    street_number = json['street_number'];
    postal_code = json['postal_code'];
    municipality = json['municipality'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['id'] = this.id;
    data['title'] = this.title;
    data['body'] = this.body;
    data['cin'] = this.cin;
    data['tin'] = this.tin;
    data['vatin'] = this.vatin;
    data['name'] = this.name;
    data['street'] = this.street;
    data['street_number'] = this.street_number;
    data['postal_code'] = this.postal_code;
    data['municipality'] = this.municipality;
    return data;
  }
}