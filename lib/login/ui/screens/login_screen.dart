import 'package:ariadna/fininfo/ui/screens/fininfo_screen.dart';
import 'package:ariadna/post/ui/screens/posts_screen.dart';
import 'package:flutter/material.dart';
import 'package:ariadna/base/bloc/bloc_provider.dart';
import 'package:ariadna/login/bloc/user_bloc.dart';
import 'package:ariadna/login/repo/user.dart';
import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/movie/ui/widgets/loading_progress_bar.dart';
import 'package:ariadna/movie/ui/views/movie_screen.dart';
import 'package:ariadna/app_localizations.dart';
import 'package:ariadna/base/ui/widgets/PillShapedButton.dart';
import 'package:flutter/cupertino.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _controller = TextEditingController();
  final TextEditingController _controlpsw = TextEditingController();

  double _height = 50;
  double _width = 150;
  bool isLoading = false;
  bool _obscured = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();

    setState(() {});

    _controller.text = "";
    _controlpsw.text = "";
  }

  @override
  void dispose() {
    _controller?.dispose();
    _controlpsw?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ApiResponse<List<User>>>(
        stream: BlocProvider
            .of<UserBloc>(context)
            .userStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            //print("Andrejko MainScreen snapshot.hasData " + snapshot.data.status.toString()  + snapshot.data.message.toString() );
            switch (snapshot.data.status) {
              case Status.LOADING:
                return Scaffold(
                    body: LoadingProgressBar(
                        loadingMessage: snapshot.data.message));
                break;
              case Status.COMPLETED:
                if (snapshot.data.data[0].jwt == null || snapshot.data.data[0].jwt == "") {
                  return Scaffold(
                      appBar: AppBar(elevation: 0.0, title: Text('Ariadna')),
                      body: _buildLoginBody(context, "", 0.00));
                } else {
                  return FinInfoScreen();
                  //return MovieScreen();
                  //return TestScreen();
                  //return PostsScreen();
                }

                break;
              case Status.ERROR:
                return Scaffold(
                    appBar: AppBar(elevation: 0.0, title: Text('Ariadna')),
                    body: _buildLoginBody(
                        context, snapshot.data.message, 1.00));

//                return Scaffold(
//                    body: ErrorDialog(
//                  errorMessage: snapshot.data.message,
//                  onRetryPressed: () {
//                    final userBloc = BlocProvider.of<UserBloc>(context);
//                    userBloc.getNextUser();
//                  },
//                ));
                break;
            }
          }
          return Container();
        });
  }

  Widget _buildLoginBody(BuildContext context, String errorMessage, double errorOpacity) {
    String _errorMessageTrans=errorMessage;
    if(errorMessage == "nomatch_userpswd") {
      _errorMessageTrans=AppLocalizations.of(context).translate('nomatch_userpswd') + " ";
    }

    if(errorMessage == "Error During Communication: No Internet connection") {
      _errorMessageTrans=AppLocalizations.of(context).translate('no_internet_connection') + " ";
    }

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Color(0xA6F6F1), Color(0x46BF58), Color(0xFF035762)],
              stops: [0, 0.35, 1.0])),
      child: ListView(
        children: [
          Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(height: 10),
                Container(
                  width: 200,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AppLocalizations.of(context).translate('login_user') +
                            " ",
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xff000000),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(height: 10),
                Container(
                  width: 185,
                  child: TextField(
                    controller: _controller,
                    autofocus: true,
                    decoration: InputDecoration(
                      labelText: AppLocalizations.of(context)
                          .translate('label_username'),
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontSize: 16,
                        color: Color(0xff9b9b9b),
                      ),
                      hintText: AppLocalizations.of(context)
                          .translate('hint_username'),
                      hintMaxLines: 1,
                    ),
                  ),
                ),
                Container(
                  width: 185,
                  child: TextField(
                    controller: _controlpsw,
                    autofocus: true,
                    obscureText: _obscured,
                    decoration: InputDecoration(
                        labelText: AppLocalizations.of(context)
                            .translate('label_password'),
                        border: InputBorder.none,
                        hintStyle: TextStyle(
                          fontSize: 16,
                          color: Color(0xff9b9b9b),
                        ),
                        hintText: AppLocalizations.of(context)
                            .translate('hint_password'),
                        hintMaxLines: 1,
                        suffixIcon: _obscured ? IconButton(
                          icon: Icon(CupertinoIcons.eye_fill),
                          onPressed: () {
                            setState(() {
                              _obscured = !_obscured;
                            });
                          },
                        ):
                        IconButton(
                          icon: Icon(CupertinoIcons.eye_slash_fill),
                          onPressed: () {
                            setState(() {
                              _obscured = !_obscured;
                            });
                          },
                        )
                    ),
                  ),
                ),
                Container(
                  height: 20,
                  child: Opacity(
                      opacity: errorOpacity,
                      child: Text(
                        _errorMessageTrans + " " + " ",
                        style: TextStyle(color: Colors.red,),
                      )),
                ),
                Container(height: 5),
                AnimatedContainer(
                  height: _height,
                  width: _width,
                  duration: Duration(milliseconds: 100),
                  curve: Curves.fastOutSlowIn,
                  child: Hero(
                    tag: "heroChoose",
                    child: PillShapedButton(
                      title:
                      AppLocalizations.of(context).translate('login') + " ",
                      onPressed: () {
                        //https://medium.com/trueface-ai/intro-to-flutter-for-android-developers-e6d5ba7dab05
                        //I/Choreographer(1378): Skipped 55 frames!  The application may be doing too much work on its main thread.
                        // you don't want to update non-existent widget
                        if (!mounted) return;
                        setState(() {
                          isLoading = true;
                          _height = 50;
                          _width = 200;
                        });

                        _loginUser(context);
                      },
                    ),
                  ),
                ),
                Container(height: 10),
              ]),
        ],
      ),
    );
  }

  void _loginUser(BuildContext context) async {
    final userBloc = BlocProvider.of<UserBloc>(context);

    await userBloc.loginUser(_controller.text, _controlpsw.text);
    //print("Andrejko LoginScreen jwt " + jwt);
    setState(() {
      isLoading = false;
      _height = 50;
      _width = 150;
    });
  }
}
