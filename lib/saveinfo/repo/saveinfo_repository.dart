import 'package:ariadna/base/api/api_base_helper.dart';
import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'package:ariadna/fininfo/repo/fininfo_response.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/fininfo/util/constants.dart';
import 'package:ariadna/saveinfo/util/constants.dart' as saveinfo;
import 'package:ariadna/base/localStoragePlugin/local_storage_provider.dart';

class SaveinfoRepository {

  ApiBaseHelper helper = locator<ApiBaseHelper>();
  AppSharedPreferences prefs = locator<AppSharedPreferences>();

  Future<List<Fininfo>> fetchDigiSkFininfoList(String name) async {
    final response = await helper.get(Constants.EKOSYSDIG_DOMAIN, "corporate_bodies/search?q=name:$name" + "&private_access_token=" + Constants.EKOSYSDIG_API_KEY);
    return FininfoResponse.fromJson(response).results;
  }

  Future<bool> saveFir(String jwtid, Fininfo info, String xfir) async {
    print("Andrejko FirRepository saveFir ");
    try {
      final response = await helper.saveFir(saveinfo.Constants.SAVEINFO_URL, jwtid, info, xfir);
      print("Andrejko FirRepository response " + response.body.toString());
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<String> getDeviceId() async {
    return prefs.getDeviceId();
  }

  Future<String> getJwt(String userId, String key) async {
    String _deviceId = await getDeviceId();
    return prefs.getString(_deviceId, key);
  }

  Future<String> getFir(String userId, String key) async {
    String _deviceId = await getDeviceId();
    return prefs.getString(_deviceId, key);
  }

}
