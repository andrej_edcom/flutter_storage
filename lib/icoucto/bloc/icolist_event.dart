part of 'icolist_bloc.dart';

@immutable
abstract class IcoListEvent {}

class GetIcoList extends IcoListEvent {
  final String cityName;

  GetIcoList(this.cityName);
}