import 'dart:math';
import 'package:ariadna/base/model/ico.dart';
import 'model/weather.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/api/api_base_helper.dart';
import 'package:ariadna/icoucto/util/constants.dart';
import 'dart:convert';
import 'package:ariadna/base/localStoragePlugin/local_storage_provider.dart';

abstract class IcolistRepository {
  /// Throws [NetworkException].
  Future<Weather> fetchIcolist(String cityName);

  Future<List<Weather>> loadIcolist(String cityName);

  Future<List<Ico>> loadListIco(String cityName);

  Future<List<Ico>> loadListIcoFromUrlbyName(String name, String jwtid, String firx);

  Future<List<Ico>> loadListIcoFromUrlbyIco(String ico, String jwtid, String firx);

  Future<List<Ico>> saveIcoToUrl(Ico ico, String jwtid, String firx);

  Future<String> getDeviceId();

  Future<String> getFir(String userId, String key);

  Future<String> getJwt(String userId, String key);

}

class FakeIcolistRepository implements IcolistRepository {

  ApiBaseHelper helper = locator<ApiBaseHelper>();
  AppSharedPreferences prefs = locator<AppSharedPreferences>();

  @override
  Future<Weather> fetchIcolist(String cityName) {
    // Simulate network delay
    return Future.delayed(
      Duration(seconds: 1),
      () {
        final random = Random();

        // Simulate some network exception
        if (random.nextBool()) {
          throw NetworkException();
        }

        // Return "fetched" weather
        return Weather(
          cityName: cityName,
          // Temperature between 20 and 35.99
          temperatureCelsius: 20 + random.nextInt(15) + random.nextDouble(),
        );
      },
    );
  }

  @override
  Future<List<Weather>> loadIcolist(String cityName) {
    // Simulate network delay
    return Future.delayed(
      Duration(seconds: 1),
          () {

        // Return "fetched" List<Weather>
        return [
          Weather( cityName: cityName, temperatureCelsius: 22,),
          Weather( cityName: cityName + "xx", temperatureCelsius: 25,)
        ];
      },
    );
  }

  @override
  Future<List<Ico>> loadListIco(String cityName) {
    // Simulate network delay
    return Future.delayed(
      Duration(seconds: 1),
          () {

        // Return "fetched" List<Ico>
        return [
          Ico(ico: "", dic: "", icd: "", nai: cityName, uli: "22"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),
          Ico(ico: "", dic: "", icd: "", nai: cityName + "xx1", uli: "25"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),
          Ico(ico: "", dic: "", icd: "", nai: cityName + "xx2", uli: "25"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),
          Ico(ico: "", dic: "", icd: "", nai: cityName + "xx3", uli: "25"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),
          Ico(ico: "", dic: "", icd: "", nai: cityName + "xx4", uli: "25"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),
          Ico(ico: "", dic: "", icd: "", nai: cityName + "xx5", uli: "25"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),
          Ico(ico: "", dic: "", icd: "", nai: cityName + "xx6", uli: "25"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),
          Ico(ico: "", dic: "", icd: "", nai: cityName + "xx7", uli: "25"
              , mes: "", psc: "", tel: "", email: "", ib1: "", sw1: ""),

        ];
      },
    );
  }

  @override
  Future<List<Ico>> loadListIcoFromUrlbyName(String name, String jwtid, String firx) async {
    try {
      final response = await helper.loadIcosFromUrlName(Constants.ICOUCTO_URL, name, jwtid, firx);
      print("Andrejko IcoListRepository response " + response.body.toString());
      if (response.statusCode == 200) {
        final result = json.decode(response.body);
        Iterable list = result['data'];
        print("Andrejko IcoListRepository result['data'] " + result['data'].toString());
        return list.map((model) => Ico.fromJson(model)).toList();
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e);
      throw NetworkException();
    }
  }

  @override
  Future<List<Ico>> loadListIcoFromUrlbyIco(String ico, String jwtid, String firx) async {
    try {
      final response = await helper.loadIcosFromUrlIco(Constants.ICOUCTO_URL, ico, jwtid, firx);
      print("Andrejko IcoListRepository response " + response.body.toString());
      if (response.statusCode == 200) {
        final result = json.decode(response.body);
        Iterable list = result['data'];
        print("Andrejko IcoListRepository result['data'] " + result['data'].toString());
        return list.map((model) => Ico.fromJson(model)).toList();
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e);
      throw NetworkException();
    }
  }

  @override
  Future<List<Ico>> saveIcoToUrl(Ico ico, String jwtid, String firx) async {
    try {
      final response = await helper.saveIcoToUrlIco(Constants.ICOUCTO_URL, ico, jwtid, firx);
      print("Andrejko IcoListRepository response " + response.body.toString());
      if (response.statusCode == 200) {
        final result = json.decode(response.body);
        Iterable list = result['data'];
        print("Andrejko IcoListRepository result['data'] " + result['data'].toString());
        return list.map((model) => Ico.fromJson(model)).toList();
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e);
      throw NetworkException();
    }
  }

  @override
  Future<String> getDeviceId() async {
    return prefs.getDeviceId();
  }

  @override
  Future<String> getFir(String userId, String key) async {
    String _deviceId = await getDeviceId();
    return prefs.getString(_deviceId, key);
  }

  @override
  Future<String> getJwt(String userId, String key) async {
    String _deviceId = await getDeviceId();
    return prefs.getString(_deviceId, key);
  }

}

class NetworkException implements Exception {}
