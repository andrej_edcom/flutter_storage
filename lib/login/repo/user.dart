import 'dart:typed_data';

class User {
  String name;
  String pswd;
  String id;
  String jwt;
  String uzid;
  String photoUrl;

  Uint8List photo;

  User({this.name, this.pswd, this.id, this.jwt, this.uzid, this.photoUrl, this.photo});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json['name'] as String,
      pswd: json['pswd'] as String,
      id: json['id'] as String,
      jwt: json['jwt'] as String,
      uzid: json['uzid'] as String,
    );
  }

}
