part of 'icoedit_bloc.dart';

@immutable
abstract class IcoEditState extends Equatable {
  const IcoEditState();
}

class IcoEditInitial extends IcoEditState {
  const IcoEditInitial();

  @override
  List<Object> get props => [];
}

class IcoEditLoading extends IcoEditState {
  const IcoEditLoading();

  @override
  List<Object> get props => [];
}

class IcoEditLoaded extends IcoEditState {
  final List<Ico> listIco;
  const IcoEditLoaded(this.listIco);

  @override
  List<Object> get props => [listIco];
}

class IcoEditError extends IcoEditState {
  final String message;
  const IcoEditError(this.message);

  @override
  List<Object> get props => [message];
}