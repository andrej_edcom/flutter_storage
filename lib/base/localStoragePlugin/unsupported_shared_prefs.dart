import 'dart:typed_data';

/*
LocalKeyValuePersistence to SharedPreferences
 */
class AppSharedPreferences {

  String _generateKey(String userId, String key) {
    return '$userId/$key';
  }

  Future<String> saveImage(String userId, String key, Uint8List image) async {

    return null;
  }

  void saveObject(
      String userId, String key, Map<String, dynamic> object) async {

  }

  void saveString(String userId, String key, String value, [String jwtid]) async {

  }

  Future<Uint8List> getImage(String userId, String key) async {

    return null;
  }

  Future<Map<String, dynamic>> getObject(String userId, String key) async {

    return null;
  }

  Future<String> getString(String userId, String key, [String jwtid]) async {

    return null;
  }

  Future<void> removeImage(String userId, String key) async {

  }

  Future<void> removeObject(String userId, String key) async {

  }

  Future<void> removeString(String userId, String key, [String jwtid]) async {

  }

  Future<String> getDeviceId() async {

    return "";
  }

}
