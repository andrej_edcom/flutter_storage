part of 'icolist_bloc.dart';

@immutable
abstract class IcoListState extends Equatable {
  const IcoListState();
}

class IcoListInitial extends IcoListState {
  const IcoListInitial();

  @override
  List<Object> get props => [];
}

class IcoListLoading extends IcoListState {
  const IcoListLoading();

  @override
  List<Object> get props => [];
}

class IcoListLoaded extends IcoListState {
  final List<Ico> listIco;
  final String cityName;
  const IcoListLoaded(this.listIco, this.cityName);

  @override
  List<Object> get props => [listIco, cityName];
}

class IcoListError extends IcoListState {
  final String message;
  const IcoListError(this.message);

  @override
  List<Object> get props => [message];
}