import 'dart:async';
import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/firucto/repo/fir_repository.dart';
import 'package:ariadna/firucto/repo/fir.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/bloc/bloc.dart';

class FirBloc implements Bloc {
  FirRepository firRepository;

  StreamController _firListController;

  StreamSink<ApiResponse<List<Fir>>> get firListSink =>
      _firListController.sink;

  Stream<ApiResponse<List<Fir>>> get firListStream =>
      _firListController.stream;

  FirBloc() {
    _firListController = StreamController<ApiResponse<List<Fir>>>();

    firRepository = locator<FirRepository>();
    fetchFirList();
  }

  fetchAFirList() async {
    String jwt = await getJwt();
    print("Andrejko FirBloc jwt " + jwt);

    firListSink.add(ApiResponse.loading('Fetching Popular Firs'));
    try {
      List<Fir> movies = await firRepository.fetchMovieList();
      firListSink.add(ApiResponse.completed(movies));
    } catch (e) {
      firListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  fetchFirList() async {
    String jwt = await getJwt();
    print("Andrejko FirBloc jwt " + jwt);

    firListSink.add(ApiResponse.loading('Fetching Popular Firs'));
    try {
      List<Fir> firms = await firRepository.loadFirs(jwt);
      //print("Andrejko FirBloc response " + firms);
      firListSink.add(ApiResponse.completed(firms));
    } catch (e) {
      firListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  Future<String> deviceId() async {
    return firRepository.getDeviceId();
  }

  Future<String> getJwt() async {
    String _deviceId = await deviceId();
    String jwt = await firRepository.getJwt(_deviceId, 'user.jwt');
    if(jwt == null) { jwt = ""; }
    return jwt;
  }

  @override
  void dispose() {
    _firListController?.close();
  }
}
