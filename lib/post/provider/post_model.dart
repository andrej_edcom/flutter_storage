import 'package:ariadna/post/repo/post_repository.dart';
import 'package:ariadna/post/repo/post_response.dart';
import 'package:flutter/foundation.dart';
import 'package:ariadna/post/repo/post.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/provider/base_model.dart';
import 'package:ariadna/base/provider/viewstate.dart';
import 'package:ariadna/base/api/api_response.dart';

class PostModel extends BaseModel {
  PostRepository postRepository = locator<PostRepository>();

  List<Post> posts = List<Post>();

  Future getPosts(int userId) async {
    debugPrint('Andrejko data: $userId');
    setState(ViewState.Busy);
    posts = await postRepository.fetchPostList(userId);
    setState(ViewState.Idle);
  }

  ApiResponse apiResponsePosts = ApiResponse.loading('Fetching User Posts');

  Future getApiResponsePosts(int userId) async {
    debugPrint('Andrejko data: $userId');
    setState(ViewState.Busy);
    try {
      List<Post> posts = await postRepository.fetchPostList(userId);
      apiResponsePosts = ApiResponse.completed(posts);
    } catch (e) {
      apiResponsePosts = ApiResponse.error(e.toString());
      print(e);
    }
    setState(ViewState.Idle);
  }

}