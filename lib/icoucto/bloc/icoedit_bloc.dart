import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:ariadna/base/model/ico.dart';
import 'package:ariadna/icoucto/data/icolist_repository.dart';
import 'package:equatable/equatable.dart';

part 'icoedit_event.dart';
part 'icoedit_state.dart';

class IcoEditBloc extends Bloc<IcoEditEvent, IcoEditState> {
  final IcolistRepository _icolistRepository;

  IcoEditBloc(this._icolistRepository) : super(IcoEditInitial());

  @override
  Stream<IcoEditState> mapEventToState(
    IcoEditEvent event,
  ) async* {
    if (event is SaveIco) {
      try {
        yield IcoEditLoading();

        String firx = await getFiructo();
        String jwtid = await getJwt();

        List<Ico> listIco = await _icolistRepository.saveIcoToUrl(event.updateIco, jwtid, firx);

        yield IcoEditLoaded(listIco);
      } on NetworkException {
        yield IcoEditError("network_error");
      }
    }
  }

  Future<String> deviceId() async {
    return _icolistRepository.getDeviceId();
  }

  Future<String> getFiructo() async {
    String _deviceId = await deviceId();
    String xfir = await _icolistRepository.getFir(_deviceId, 'user.fir');
    if (xfir == null) {
      xfir = "";
    }
    return xfir;
  }

  Future<String> getJwt() async {
    String _deviceId = await deviceId();
    String jwt = await _icolistRepository.getJwt(_deviceId, 'user.jwt');
    if (jwt == null) {
      jwt = "";
    }
    return jwt;
  }
}
