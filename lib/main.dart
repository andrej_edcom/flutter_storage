import 'package:ariadna/fininfo/ui/screens/fininfo_screen.dart';
import 'package:ariadna/icoucto/screens/icolist_screen.dart';
import 'package:ariadna/weather/pages/weather_search_blocpage.dart';
import 'package:ariadna/weather/pages/weather_search_cubitpage.dart';
import 'package:flutter/material.dart';
import 'locator.dart';
import 'package:ariadna/base/bloc/bloc_provider.dart';
import 'package:ariadna/login/bloc/user_bloc.dart';
import 'package:ariadna/login/ui/screens/login_screen_animated.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app_localizations.dart';
import 'navigator.dart';
import 'package:ariadna/firucto/bloc/firucto_bloc.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserBloc>(
      bloc: locator<UserBloc>(),
      child: BlocProvider<FiructoBloc>(
        bloc: locator<FiructoBloc>(),
        child: MaterialApp(
          supportedLocales: [
            Locale('en', 'US'),
            Locale('sk', ''),
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          debugShowCheckedModeBanner: false,
          title: 'Ariadna',
          theme: ThemeData(
              primaryColor: Color(0xff006837),
              primaryColorDark: Color(0xff004012),
              accentColor: Color(0xffc75f00)),
            //home: IcolistScreen(),
            //home: FinInfoScreen(),
            home: LoginScreen(),
            //home: WeatherSearchBlocPage(),
            //home: WeatherSearchCubitPage(),
          onGenerateRoute: (settings) {
            return buildPageRouteBuilder(context, settings);
          }),
    ),
    );
  }
}
