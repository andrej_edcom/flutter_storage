import 'dart:typed_data';
import 'dart:html' as html;

/*
LocalKeyValuePersistence to SharedPreferences
 */
class AppSharedPreferences {

  String _generateKey(String userId, String key) {
    return '$userId/$key';
  }

  Future<String> saveImage(String userId, String key, Uint8List image) async {

    return null;
  }

  void saveObject(
      String userId, String key, Map<String, dynamic> object) async {

  }

  void saveString(String userId, String key, String value, [String jwtid]) async {
    html.window.document.cookie =
    "${key}=${value}; expires=Thu, 18 Dec 2022 12:00:00 UTC";
  }

  Future<Uint8List> getImage(String userId, String key) async {

    return null;
  }

  Future<Map<String, dynamic>> getObject(String userId, String key) async {

    return null;
  }

  Future<String> getString(String userId, String key, [String jwtid]) async {

    String cookies = html.window.document.cookie;
    List<String> listValues = cookies.isNotEmpty ? cookies.split(";") : List();
    String matchVal = "";
    for (int i = 0; i < listValues.length; i++) {
      List<String> map = listValues[i].split("=");
      String _key = map[0].trim();
      String _val = map[1].trim();
      if (key == _key) {
        matchVal = _val;
        break;
      }
    }
    return matchVal;
  }

  Future<void> removeImage(String userId, String key) async {

  }

  Future<void> removeObject(String userId, String key) async {

  }

  Future<void> removeString(String userId, String key, [String jwtid]) async {

  }

  Future<String> getDeviceId() async {

    return "";
  }

}
