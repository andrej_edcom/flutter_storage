import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ariadna/base/provider/base_model.dart';
import '../../locator.dart';

class BaseViewProvider<T extends BaseModel> extends StatefulWidget {
  final Widget Function(BuildContext context, T model, Widget child) builder;
  final Function(T) onModelReady;

  BaseViewProvider({this.builder, this.onModelReady});

  @override
  _BaseViewProviderState<T> createState() => _BaseViewProviderState<T>();
}

class _BaseViewProviderState<T extends BaseModel> extends State<BaseViewProvider<T>> {
  T model = locator<T>();

  @override
  void initState() {
    if (widget.onModelReady != null) {
      widget.onModelReady(model);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
        create: (context) => model,
        child: Consumer<T>(builder: widget.builder));
  }
}
