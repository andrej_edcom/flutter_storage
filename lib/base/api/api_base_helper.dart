import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:ariadna/base/exceptions/app_exceptions.dart';
import 'dart:async';
import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'package:ariadna/base/model/ico.dart';

class ApiBaseHelper {

  Future<dynamic> get(String domain, String url) async {
    print('Api Get, url $domain + $url');
    var responseJson;
    try {
      final response = await http.get(domain + url);
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }
    print('api get recieved!');
    return responseJson;
  }

  Future<dynamic> post(String domain, String url, dynamic body) async {
    print('Api Post, url $domain + $url');
    var responseJson;
    try {
      final response = await http.post(domain + url, body: body);
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }
    print('api post.');
    return responseJson;
  }

  Future<dynamic> put(String domain, String url, dynamic body) async {
    print('Api Put, url $domain + $url');
    var responseJson;
    try {
      final response = await http.put(domain + url, body: body);
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }
    print('api put.');
    print(responseJson.toString());
    return responseJson;
  }

  Future<dynamic> delete(String domain, String url) async {
    print('Api delete, url $domain + $url');
    var apiResponse;
    try {
      final response = await http.delete(domain + url);
      apiResponse = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }
    print('api delete.');
    return apiResponse;
  }

  Future<String> attemptLogIn(String url, String username, String pswd) async {

    var res = null;
    try {
      res = await http.get(url +
          "?username=" +
          username +
          "&password=" +
          pswd +
          "&psysx=1");
    } on SocketException {
      throw FetchDataException('No Internet connection');
      return "";
    }

    if (res.statusCode == 200) return res.body;
    return "";
  }

  Future<http.Response> loadFirsFromUrl(String url, String jwtid) async {

    return http.get(url + "?jwtid=" + jwtid);
  }

  Future<http.Response> saveFir(String url, String jwtid, Fininfo info, String xfir) async {

    print("Andrejko helper saveFir " + info.toJson().toString());
    return http.get(url + "?jwtid=" + jwtid + "&firx=" + xfir + "&icox=" + info.cin
        + "&naix=" + getNotNullString(info.name)
        + "&dicx=" + getNotNullString(info.tin.toString())
        + "&icdx=" + getNotNullString(info.vatin)
        + "&ulix=" + getNotNullString(info.street)
        + "&cdmx=" + getNotNullString(info.street_number)
        + "&pscx=" + getNotNullString(info.postal_code)
        + "&mesx=" + getNotNullString(info.municipality));
  }

  String getNotNullString(String strx) {
    if (strx == null) {strx="";}
    return strx;
  }

  Future<http.Response> loadIcosFromUrlName(String url, String name, String jwtid, String firx) async {

    return http.get(url + "?jwtid=" + jwtid + "&firx=" + firx + "&name=" + name);
  }

  Future<http.Response> loadIcosFromUrlIco(String url, String ico, String jwtid, String firx) async {

    return http.get(url + "?jwtid=" + jwtid + "&firx=" + firx + "&ico=" + ico);
  }

  Future<http.Response> saveIcoToUrlIco(String url, Ico ico, String jwtid, String firx) async {

    return http.get(url + "?jwtid=" + jwtid + "&firx=" + firx + "&ico=" + ico.ico + "&copern=7"
        + "&naix=" + getNotNullString(ico.nai)
        + "&dicx=" + getNotNullString(ico.dic)
        + "&icdx=" + getNotNullString(ico.icd)
        + "&ulix=" + getNotNullString(ico.uli)
        + "&pscx=" + getNotNullString(ico.psc)
        + "&mesx=" + getNotNullString(ico.mes)
        + "&telx=" + getNotNullString(ico.tel)
        + "&emailx=" + getNotNullString(ico.email)
        + "&ib1x=" + getNotNullString(ico.ib1)
        + "&sw1x=" + getNotNullString(ico.sw1)
    );
  }

}

dynamic _returnResponse(http.Response response) {
  switch (response.statusCode) {
    case 200:
      var responseJson = json.decode(response.body.toString());
      print(responseJson);
      return responseJson;
    case 400:
      throw BadRequestException(response.body.toString());
    case 401:
    case 403:
      throw UnauthorisedException(response.body.toString());
    case 500:
    default:
      throw FetchDataException(
          'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
  }

}
