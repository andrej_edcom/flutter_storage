class Fir {
  String fir;
  String name;
  String year;

  Fir({
    this.fir,
    this.name,
    this.year,
  });

  Fir.fromJson(Map<String, dynamic> json) {
    fir = json['xcf'];
    name = json['naz'];
    year = json['rok'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['xcf'] = this.fir;
    data['naz'] = this.name;
    data['rok'] = this.year;
    return data;
  }
}
