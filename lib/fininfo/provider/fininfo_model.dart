import 'package:ariadna/fininfo/repo/fininfo_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/provider/base_model.dart';
import 'package:ariadna/base/provider/viewstate.dart';
import 'package:ariadna/base/api/api_response.dart';
import 'package:validators/validators.dart' as validator;

class FininfoModel extends BaseModel {
  FininfoRepository fininfoRepository = locator<FininfoRepository>();

  ApiResponse apiResponseFininfos = ApiResponse.loading('Fetching Fininfo');

  Future getApiResponsePosts(int userId) async {
    debugPrint('Andrejko data: $userId');

    apiResponseFininfos = ApiResponse.loading("");
    setState(ViewState.Busy);
    try {
      List<Fininfo> fins = await fininfoRepository.fetchFininfoList(userId);
      apiResponseFininfos = ApiResponse.completed(fins);
    } catch (e) {
      apiResponseFininfos = ApiResponse.error(e.toString());
      print(e);
    }
    setState(ViewState.Idle);
  }

  Future getApiResponseFininfos(String name) async {
    debugPrint('Andrejko data: $name');

    bool _find = false;
    bool _fico = false;
    if( name.length > 3 ) { _find = true; }
    if( validator.isNumeric(name) && (validator.isLength(name, 6) || validator.isLength(name, 8))) { _fico = true; }

    if( _find ) {
      apiResponseFininfos = ApiResponse.loading("");
      setState(ViewState.Busy);
      List<Fininfo> fins = [];
      try {
        if(_fico) {
          fins = await fininfoRepository.fetchDigiSkIcoFininfoList(name);
        } else {
          fins = await fininfoRepository.fetchDigiSkFininfoList(name);
        }
        apiResponseFininfos = ApiResponse.completed(fins);
      } catch (e) {
        apiResponseFininfos = ApiResponse.error(e.toString());
        print(e);
      }
      setState(ViewState.Idle);
    }

  }

  Future initFininfo() async {
    List<Fininfo> _fininfos = [];
    apiResponseFininfos = ApiResponse.completed(_fininfos);
  }

  Future demoFininfo() async {
    final fininfo1 = Fininfo(id: 1, cin: "17 330 581", tin:  2020342511 , vatin: "SK 2020342511",
      name: "Coex spol. s r.o.", street: "Sotinská", street_number: "1474/11",
      postal_code: "90501", municipality: "Senica");
    final fininfo2 = Fininfo(id: 2, cin: "17 330 581", tin:  2020342511 , vatin: "SK 2020342511",
        name: "Coex2 spol. s r.o.", street: "Sotinská", street_number: "1474/11",
        postal_code: "90501", municipality: "Senica");
    final fininfo3 = Fininfo(id: 3, cin: "17 330 581", tin:  2020342511 , vatin: "SK 2020342511",
        name: "Coex3 spol. s r.o.", street: "Sotinská", street_number: "1474/11",
        postal_code: "90501", municipality: "Senica");
    final fininfo4 = Fininfo(id: 4, cin: "17 330 581", tin:  2020342511 , vatin: "SK 2020342511",
        name: "Coex4 spol. s r.o.", street: "Sotinská", street_number: "1474/11",
        postal_code: "90501", municipality: "Senica");
    final fininfo5 = Fininfo(id: 5, cin: "17 330 581", tin:  2020342511 , vatin: "SK 2020342511",
        name: "Coex5 spol. s r.o.", street: "Sotinská", street_number: "1474/11",
        postal_code: "90501", municipality: "Senica");
    List<Fininfo> _fininfos = [fininfo1, fininfo2, fininfo3, fininfo4, fininfo5];
    apiResponseFininfos = ApiResponse.completed(_fininfos);
  }

}