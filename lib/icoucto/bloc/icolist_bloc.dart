import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:ariadna/base/model/ico.dart';
import 'package:ariadna/icoucto/data/icolist_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:validators/validators.dart' as validator;
import 'package:ariadna/locator.dart';

part 'icolist_event.dart';
part 'icolist_state.dart';

class IcoListBloc extends Bloc<IcoListEvent, IcoListState> {
  final IcolistRepository _icolistRepository;

  //initiale Bloc by GetIt locator
  IcoListBloc() : _icolistRepository = locator<FakeIcolistRepository>(), super(IcoListInitial());

  //initiale Bloc without GetIt locator
  //IcoListBloc(this._icolistRepository) : super(IcoListInitial());

  @override
  Stream<IcoListState> mapEventToState(
      IcoListEvent event,
      ) async* {
    if (event is GetIcoList) {
      try {
        yield IcoListLoading();
        //final listIco = await _icolistRepository.loadListIco(event.cityName);

        bool _fico = false;
        if( validator.isNumeric(event.cityName) && (validator.isLength(event.cityName, 6)
            || validator.isLength(event.cityName, 8))) { _fico = true; }

        String firx = await getFiructo();
        if(firx == "") {
          yield IcoListError("choose_firucto");
        } else {
          String jwtid = await getJwt();

          List<Ico> listIco = [];
          if(!_fico) {
            listIco = await _icolistRepository.loadListIcoFromUrlbyName(event.cityName, jwtid, firx);
          } else {
            listIco = await _icolistRepository.loadListIcoFromUrlbyIco(event.cityName, jwtid, firx);
          }

          yield IcoListLoaded(listIco, event.cityName);
        }

      } on NetworkException {
        yield IcoListError("network_error");
      }
    }
  }

  Future<String> deviceId() async {
    return _icolistRepository.getDeviceId();
  }

  Future<String> getFiructo() async {
    String _deviceId = await deviceId();
    String xfir = await _icolistRepository.getFir(_deviceId, 'user.fir');
    if(xfir == null) { xfir = ""; }
    return xfir;
  }

  Future<String> getJwt() async {
    String _deviceId = await deviceId();
    String jwt = await _icolistRepository.getJwt(_deviceId, 'user.jwt');
    if(jwt == null) { jwt = ""; }
    return jwt;
  }

}