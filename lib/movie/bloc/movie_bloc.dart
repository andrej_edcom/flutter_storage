import 'dart:async';

import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/movie/repo/movie_repository.dart';
import 'package:ariadna/movie/repo/movie_response.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/bloc/bloc.dart';

class MovieBloc implements Bloc {
  MovieRepository movieRepository;

  StreamController _movieListController;

  StreamSink<ApiResponse<List<Movie>>> get movieListSink =>
      _movieListController.sink;

  Stream<ApiResponse<List<Movie>>> get movieListStream =>
      _movieListController.stream;

  MovieBloc() {
    _movieListController = StreamController<ApiResponse<List<Movie>>>();

    //without DI locator GetIt
    //_movieRepository = MovieRepository();

    //with DI locator GetIt is public because of testing mock
    movieRepository = locator<MovieRepository>();
    fetchMovieList();
  }

  fetchMovieList() async {
    movieListSink.add(ApiResponse.loading('Fetching Popular Movies'));
    try {
      List<Movie> movies = await movieRepository.fetchMovieList();
      movieListSink.add(ApiResponse.completed(movies));
    } catch (e) {
      movieListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  @override
  void dispose() {
    _movieListController?.close();
  }
}
