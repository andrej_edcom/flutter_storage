import 'package:flutter/material.dart';
import 'package:ariadna/fininfo/repo/fininfo.dart';

class FininfoListItem extends StatefulWidget {
  final Fininfo fininfo;
  final Function onTap;

  const FininfoListItem({this.fininfo, this.onTap});

  @override
  _FininfoListItemState createState() => _FininfoListItemState();
}

class _FininfoListItemState extends State<FininfoListItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                  blurRadius: 3.0,
                  offset: Offset(0.0, 2.0),
                  color: Color.fromARGB(80, 0, 0, 0))
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Hero(
                  tag: 'finHero${widget.fininfo.id}',
                  child: Icon(
                    Icons.circle,
                    color: Colors.green,
                    size: 18,
                  ),
                ),
                SizedBox(
                  width: 12,
                ),
                Text(
                  "ičo: " +
                      widget.fininfo.cin +
                      " dič: " +
                      widget.fininfo.tin.toString(),
                  style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16.0),
                )
              ],
            ),
            widget.fininfo.vatin != null
                ? Text(
                    "ičdph: " + widget.fininfo.vatin,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  )
                : Text(
                    "ičdph: ",
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
            Text(
              getDescCompany(widget.fininfo.name, widget.fininfo.street,
                  widget.fininfo.street_number, widget.fininfo.postal_code, widget.fininfo.municipality),
              style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16.0),
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }

  String getDescCompany(String name, String street, String street_number, String postal_code, String municipality) {
    String description = "";
    if(name != null) description = description + name + ", ";
    if(street != null) description = description + street + " ";
    if(street_number != null) description = description + street_number + ", ";
    if(postal_code != null) description = description + postal_code + " ";
    if(municipality != null) description = description + municipality;
    return description;
  }
}
