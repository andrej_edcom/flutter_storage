//LocalStorage implemented by condition in locator
//import 'package:ariadna/base/localStorage/local_storage.dart';
//import 'package:ariadna/base/localStorage/mobile_shared_preferences.dart';
//import 'package:ariadna/base/localStorage/web_shared_preferences.dart';
//import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:ariadna/firucto/bloc/firucto_bloc.dart';
import 'package:ariadna/firucto/repo/firucto_repository.dart';
import 'package:ariadna/firucto/bloc/fir_bloc.dart';
import 'package:ariadna/firucto/repo/fir_repository.dart';
import 'package:ariadna/login/repo/user_repository.dart';
import 'package:ariadna/movie/bloc/movie_bloc.dart';
import 'package:ariadna/movie/repo/movie_repository.dart';
import 'package:ariadna/saveinfo/provider/saveinfo_model.dart';
import 'package:ariadna/saveinfo/repo/saveinfo_repository.dart';
import 'package:get_it/get_it.dart';
import 'package:ariadna/base/api/api_base_helper.dart';
import 'package:ariadna/post/provider/post_model.dart';
import 'package:ariadna/post/repo/post_repository.dart';
import 'package:ariadna/login/bloc/user_bloc.dart';
import 'package:ariadna/base/localStoragePlugin/local_storage_provider.dart';
import 'package:ariadna/fininfo/provider/fininfo_model.dart';
import 'package:ariadna/fininfo/repo/fininfo_repository.dart';
import 'package:ariadna/icoucto/data/icolist_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ariadna/icoucto/bloc/icolist_bloc.dart';

GetIt locator = GetIt.instance;

void setupLocator() {

  //base
  locator.registerLazySingleton(() => ApiBaseHelper());

  //multiplatform LocalStorage implemented by condition in locator
//  if (kIsWeb) {
//    locator.registerLazySingleton<LocalStorage>(() => WebSharedPreferences());
//  } else {
//    locator.registerLazySingleton<LocalStorage>(() => MobileSharedPreferences());
//  }

  //multiplatform LocalStorage implemented by condition import classes
  locator.registerLazySingleton<AppSharedPreferences>(() => AppSharedPreferences());


  //movie
  locator.registerLazySingleton(() => MovieRepository());
  locator.registerFactory(() => MovieBloc());

  //post
  locator.registerLazySingleton(() => PostRepository());
  locator.registerFactory(() => PostModel());

  //login
  locator.registerLazySingleton(() => UserRepository());
  locator.registerLazySingleton(() => UserBloc());

  //Fininfo
  locator.registerLazySingleton(() => FininfoRepository());
  locator.registerFactory(() => FininfoModel());

  //Firucto
  locator.registerLazySingleton(() => FiructoRepository());
  locator.registerLazySingleton(() => FiructoBloc());
  locator.registerFactory(() => FirBloc());
  locator.registerLazySingleton(() => FirRepository());

  //saveinfo
  locator.registerLazySingleton(() => SaveinfoRepository());
  locator.registerFactory(() => SaveinfoModel());

  //icolistbloc
  locator.registerLazySingleton(() => FakeIcolistRepository());
  //initiale Bloc by GetIt locator
  locator.registerLazySingleton(() => IcoListBloc());
  //initiale Bloc without GetIt locator
  //locator.registerLazySingleton(() => IcoListBloc(FakeIcolistRepository()));

}