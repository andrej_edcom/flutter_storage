import 'package:ariadna/base/api/api_base_helper.dart';
import 'package:ariadna/post/repo/post.dart';
import 'post_response.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/post/util/constants.dart';

class PostRepository {

  //without DI locator GetIt
  //ApiBaseHelper _helper = ApiBaseHelper();

  //with DI locator GetIt
  ApiBaseHelper helper = locator<ApiBaseHelper>();

  Future<List<Post>> fetchPostList(int userId) async {
    final response = await helper.get(Constants.POST_DOMAIN, "posts?userId=$userId");
    return PostResponse.fromJson(response).results;
  }
}
