import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'package:flutter/material.dart';
import 'package:ariadna/app_localizations.dart';
import 'package:ariadna/base/ui/widgets/PillShapedButton.dart';
import 'package:ariadna/base/provider/base_view_provider.dart';
import 'package:ariadna/saveinfo/provider/saveinfo_model.dart';
import 'package:ariadna/base/ui/widgets/alert_dialog.dart';

class SaveinfoScreen extends StatefulWidget {
  final Fininfo item;

  const SaveinfoScreen({Key key, this.item}) : super(key: key);

  @override
  _SaveinfoScreenState createState() => _SaveinfoScreenState();
}

class _SaveinfoScreenState extends State<SaveinfoScreen>
    with TickerProviderStateMixin {
  Animation _heartAnimation;
  AnimationController _heartAnimationController;

  @override
  void initState() {
    super.initState();

    _heartAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1200));
    _heartAnimation = Tween(begin: 150.0, end: 200.0).animate(CurvedAnimation(
        curve: Curves.bounceOut, parent: _heartAnimationController));

    _heartAnimationController.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _heartAnimationController.repeat();
      }
    });

    _heartAnimationController.forward();
  }

  @override
  void dispose() {
    _heartAnimationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double _height = 50;

    return BaseViewProvider<SaveinfoModel>(
        onModelReady: (model) => model.initFininfo(),
        builder: (context, model, child) => Scaffold(
              appBar: AppBar(
                  elevation: 0.0, title: Text('Ariadna'), actions: <Widget>[]),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          widget.item.cin != null ? widget.item.cin + " " : "",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      widget.item.name != null
                          ? widget.item.name + " "
                          : "",
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 20),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      widget.item.tin != null
                          ? widget.item.tin.toString() + " " + " "
                          : "",
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 12),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      widget.item.vatin != null
                          ? widget.item.vatin + " " + " "
                          : "",
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 12),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      getDescCompany(
                              widget.item.street,
                              widget.item.street_number,
                              widget.item.postal_code,
                              widget.item.municipality) +
                          " " +
                          " ",
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 20),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AnimatedBuilder(
                        animation: _heartAnimationController,
                        builder: (context, snapshot) {
                          return Container(
                            height: _height,
                            width: _heartAnimation.value,
                            child: Hero(
                              tag: 'finHero${widget.item.id}',
                              child: PillShapedButton(
                                  title: AppLocalizations.of(context)
                                          .translate('save_info') +
                                      " ",
                                  onPressed: () {
                                    _heartAnimationController.stop();
                                    model
                                        .saveInfo(widget.item)
                                        .then((value) {
                                      if (value) {
                                        print(
                                            "Andrejko SaveinfoScreen value " +
                                                value.toString());
                                        displayDialog(
                                            context,
                                            "",
                                            AppLocalizations.of(context)
                                                .translate('info_saved'));
                                      } else {
                                        displayDialog(
                                            context,
                                            AppLocalizations.of(context)
                                                .translate('error_occur'),
                                            AppLocalizations.of(context)
                                                .translate(
                                                    'info_notsaved'));
                                      }
                                    });
                                  }),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ));
  }

  String getDescCompany(String street, String street_number, String postal_code,
      String municipality) {
    String description = "";
    if (street != null) description = description + street + " ";
    if (street_number != null) description = description + street_number + ", ";
    if (postal_code != null) description = description + postal_code + " ";
    if (municipality != null) description = description + municipality;
    return description;
  }
}
