import 'package:flutter/material.dart';
import 'create_drawer_header.dart';
import 'create_drawer_body_item.dart';
import 'package:ariadna/app_localizations.dart';

class navigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(
              icon: Icons.find_in_page,
              text: AppLocalizations.of(context).translate('finstat_info') + " " + " ",
              onTap: () {
                Navigator.pop(context);
                Navigator.pushReplacementNamed(context, '/fininfo');

              }
          ),
          createDrawerBodyItem(
            icon: Icons.home,
            text: AppLocalizations.of(context).translate('set_account_company') + " " + " ",
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/firucto');

            }
          ),
          createDrawerBodyItem(icon: Icons.settings_input_composite,
              text: AppLocalizations.of(context).translate('account_companies') + " " + " ",
              onTap: () {
                Navigator.pop(context);
                //problem with pushNamed does not work  Bloc pattern
                Navigator.pushReplacementNamed(context, '/icolist');

              }
          ),
          createDrawerBodyItem(icon: Icons.add_box,
            text: AppLocalizations.of(context).translate('doc_income') + " " + " ",
          ),
          createDrawerBodyItem(icon: Icons.indeterminate_check_box,
            text: AppLocalizations.of(context).translate('doc_spend') + " " + " ",
          ),
          createDrawerBodyItem(icon: Icons.list,
            text: AppLocalizations.of(context).translate('doc_common') + " " + " ",
          ),
          Divider(),
          ListTile(
            title: Text('v. 1.0.0'),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
