import 'package:ariadna/base/api/api_base_helper.dart';
import 'fir_response.dart';
import 'fir.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/firucto/util/constants.dart';
import 'dart:convert';
import 'package:ariadna/base/localStoragePlugin/local_storage_provider.dart';

class FirRepository {

  ApiBaseHelper helper = locator<ApiBaseHelper>();
  AppSharedPreferences prefs = locator<AppSharedPreferences>();

  Future<List<Fir>> fetchMovieList() async {
    final response = await helper.get(Constants.FIR_DOMAIN, "movie/popular?api_key=" + Constants.FIR_API_KEY);
    return FirResponse.fromJson(response).results;
  }

  Future<List<Fir>> loadFirs(String jwtid) async {
    try {
      final response = await helper.loadFirsFromUrl(Constants.FIRS_URL, jwtid);
      print("Andrejko FirRepository response " + response.body.toString());
      if (response.statusCode == 200) {
        final result = json.decode(response.body);
        Iterable list = result['data'];
        print("Andrejko FirRepository result['data'] " + result['data'].toString());
        return list.map((model) => Fir.fromJson(model)).toList();
      } else {
        return errorFirs("Error - No internet response.");
      }
    } catch (e) {
      print(e);
      return errorFirs(e.toString());
    }
  }

  Future<List<Fir>> errorFirs(String error) async {
    final fir1 = Fir(fir: "", name: error, year: "9999");
    List<Fir> _firs = [fir1];
    return _firs;
  }

  Future<String> getDeviceId() async {
    return prefs.getDeviceId();
  }

  Future<String> getJwt(String userId, String key) async {
    String _deviceId = await getDeviceId();
    return prefs.getString(_deviceId, key);
  }

}
