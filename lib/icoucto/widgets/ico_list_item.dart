import 'package:ariadna/base/model/ico.dart';
import 'package:flutter/material.dart';

class IcoListItem extends StatefulWidget {
  final Ico icoItem;
  final Function onTap;

  const IcoListItem({this.icoItem, this.onTap});

  @override
  _IcoListItemState createState() => _IcoListItemState();
}

class _IcoListItemState extends State<IcoListItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                  blurRadius: 3.0,
                  offset: Offset(0.0, 2.0),
                  color: Color.fromARGB(80, 0, 0, 0))
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Hero(
                  tag: 'icoHero${widget.icoItem.ico}',
                  child: Icon(
                    Icons.circle,
                    color: Colors.green,
                    size: 18,
                  ),
                ),
                SizedBox(
                  width: 12,
                ),
                Text(
                  "ičo: " +
                      widget.icoItem.ico +
                      " dič: " +
                      widget.icoItem.dic.toString(),
                  style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16.0),
                )
              ],
            ),
            widget.icoItem.icd != null
                ? Text(
              "ičdph: " + widget.icoItem.icd,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            )
                : Text(
              "ičdph: ",
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            Text(
              getDescCompany(widget.icoItem.nai, widget.icoItem.uli,
                  widget.icoItem.psc, widget.icoItem.mes),
              style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16.0),
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }

  String getDescCompany(String name, String street, String postal_code, String municipality) {
    String description = "";
    if(name != null) description = description + name + ", ";
    if(street != null) description = description + street + ", ";
    if(postal_code != null) description = description + postal_code + " ";
    if(municipality != null) description = description + municipality;
    return description;
  }
}
