import 'package:ariadna/base/model/ico.dart';
import 'package:flutter/material.dart';
import 'package:ariadna/app_localizations.dart';
import 'package:ariadna/icoucto/util/constants.dart';
import 'package:ariadna/base/ui/widgets/PillShapedButton.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ariadna/icoucto/bloc/icoedit_bloc.dart';
import 'package:ariadna/icoucto/data/icolist_repository.dart';
import 'package:ariadna/icoucto/bloc/icolist_bloc.dart';
import 'package:ariadna/locator.dart';

class IcoEditScreen extends StatefulWidget {
  final Ico item;
  final String cityName;

  const IcoEditScreen({Key key, this.item, this.cityName}) : super(key: key);

  @override
  _IcoEditScreenState createState() => _IcoEditScreenState();
}

class _IcoEditScreenState extends State<IcoEditScreen> {
  final _formKey = GlobalKey<FormState>();
  Ico _saveIco;

  @override
  void initState() {
    _saveIco = Ico();
    _saveIco.ico = widget.item.ico;
  }

  @override
  Widget build(BuildContext context) {

    IcoListBloc icoListBloc = locator<IcoListBloc>();

    return BlocProvider(
      create: (context) => IcoEditBloc(FakeIcolistRepository()),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            AppLocalizations.of(context).translate('account_companies') +
                " - " +
                widget.cityName +
                " ",
          ),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: BlocConsumer<IcoEditBloc, IcoEditState>(
            listener: (context, state) {
              if (state is IcoEditError) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context)
                            .translate(state.message) +
                        " "),
                  ),
                );
              }
              if (state is IcoEditLoaded) {
                icoListBloc.add(GetIcoList(widget.cityName));
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                        AppLocalizations.of(context).translate('ico_saved') +
                            " "),
                  ),
                );
              }
            },
            builder: (context, state) {
              return Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Hero(
                            tag: 'icoHero${widget.item.ico}',
                            child: FadeInImage.assetNetwork(
                              placeholder:
                                  Constants.ICO_PLACEHOLDER_IMAGE_ASSET_URL,
                              image: widget.item.ib1,
                              height: 90,
                              alignment: Alignment.centerLeft,
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Text(AppLocalizations.of(context)
                                    .translate('company_ico') +
                                " " +
                                widget.item.ico)),
                        Expanded(
                          flex: 1,
                          child: SaveButton(
                              formKey: _formKey, saveIco: _saveIco),
                        ),
                      ],
                    ),
                    TextFormField(
                      initialValue: widget.item.dic,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_dic')),
                      validator: (value) {
                        if (!widget.item.validDic(value)) {
                          String errx = widget.item.wrongDic();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.dic = value,
                    ),
                    TextFormField(
                      initialValue: widget.item.icd,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_icd')),
                      validator: (value) {
                        if (!widget.item.validIcd(value)) {
                          String errx = widget.item.wrongIcd();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.icd = value,
                    ),
                    TextFormField(
                      initialValue: widget.item.nai,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_nai')),
                      validator: (value) {
                        if (!widget.item.validNai(value)) {
                          String errx = widget.item.wrongNai();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.nai = value,
                    ),
                    TextFormField(
                      initialValue: widget.item.uli,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_uli')),
                      validator: (value) {
                        if (!widget.item.validUli(value)) {
                          String errx = widget.item.wrongUli();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.uli = value,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: TextFormField(
                            initialValue: widget.item.psc,
                            decoration: InputDecoration(
                                labelText: AppLocalizations.of(context)
                                    .translate('company_psc')),
                            validator: (value) {
                              if (!widget.item.validPsc(value)) {
                                String errx = widget.item.wrongPsc();
                                return AppLocalizations.of(context)
                                    .translate(errx);
                              }
                              return null;
                            },
                            onSaved: (value) => _saveIco.psc = value,
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: TextFormField(
                            initialValue: widget.item.mes,
                            decoration: InputDecoration(
                                labelText: AppLocalizations.of(context)
                                    .translate('company_mes')),
                            validator: (value) {
                              if (!widget.item.validMes(value)) {
                                String errx = widget.item.wrongMes();
                                return AppLocalizations.of(context)
                                    .translate(errx);
                              }
                              return null;
                            },
                            onSaved: (value) => _saveIco.mes = value,
                          ),
                        ),
                      ],
                    ),
                    TextFormField(
                      initialValue: widget.item.tel,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_tel')),
                      validator: (value) {
                        if (!widget.item.validTel(value)) {
                          String errx = widget.item.wrongTel();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.tel = value,
                    ),
                    TextFormField(
                      initialValue: widget.item.email,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_email')),
                      validator: (value) {
                        if (!widget.item.validEmail(value)) {
                          String errx = widget.item.wrongEmail();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.email = value,
                    ),
                    TextFormField(
                      initialValue: widget.item.ib1,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_ib1')),
                      validator: (value) {
                        if (!widget.item.validIb1(value)) {
                          String errx = widget.item.wrongIb1();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.ib1 = value,
                    ),
                    TextFormField(
                      initialValue: widget.item.sw1,
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('company_sw1')),
                      validator: (value) {
                        if (!widget.item.validSw1(value)) {
                          String errx = widget.item.wrongSw1();
                          return AppLocalizations.of(context).translate(errx);
                        }
                        return null;
                      },
                      onSaved: (value) => _saveIco.sw1 = value,
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class SaveButton extends StatelessWidget {
  const SaveButton({
    Key key,
    @required GlobalKey<FormState> formKey,
    @required Ico saveIco,
  })  : _formKey = formKey,
        _saveIco = saveIco,
        super(key: key);

  final GlobalKey<FormState> _formKey;
  final Ico _saveIco;

  @override
  Widget build(BuildContext context) {
    return PillShapedButton(
      title: AppLocalizations.of(context).translate('submit'),
      onPressed: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        if (_formKey.currentState.validate()) {
          _formKey.currentState.save();
          print("Andrejko mes " + _saveIco.mes);
          submitSaveIco(context, _saveIco);
        }
      },
    );
  }

  void submitSaveIco(BuildContext context, Ico ico) {
    FocusScope.of(context).requestFocus(new FocusNode());
    final icoEditBloc = context.read<IcoEditBloc>();
    icoEditBloc.add(SaveIco(ico));
  }
}
