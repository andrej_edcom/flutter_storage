import 'package:flutter/material.dart';

class TestScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Color(0xFF2BB88B),
                  Color(0xFF07909B),
                  Color(0xFF035762)],
                stops: [
                  0,
                  0.45,
                  1.0
                ]
            )),
      )
    );
  }
}
