import 'package:ariadna/base/api/api_response.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/movie/repo/movie_repository.dart';
import 'package:ariadna/movie/repo/movie_response.dart';
import 'package:ariadna/movie/bloc/movie_bloc.dart';

void main() {

  setupLocator();
  var movieBloc = locator<MovieBloc>();
  movieBloc.movieRepository = MockMovieRepository();

  group('MovieBloc tests', () {
    test('MovieBloc test stream response', () async {

      await movieBloc.fetchMovieList();

      int i = 0;
      movieBloc.movieListStream.listen((value) {
        i++;
        print('Value from controller: ' + value.status.toString());
          switch (value.status.toString()) {
            case "Status.LOADING":
              expect(value.status.toString(), "Status.LOADING");
              expect(value.message, "Fetching Popular Movies");
              break;
            case "Status.COMPLETED":
              expect(value.status.toString(), "Status.COMPLETED");
              expect(value.message, null);
              expect(value.data.length, 1);
              expect(value.data[0].id, 1);
              expect(value.data[0].title, "title 1");
              break;
            case "Status.ERROR":

              break;
          }
      });




    });
  });

}


class MockMovieRepository extends MovieRepository {
  @override
  Future<List<Movie>> fetchMovieList() {

    var listMovie = new List<Movie>();
    var movie = new Movie();
    movie.id = 1;
    movie.title = "title 1";
    listMovie.add(movie);
    return Future.value(listMovie);
  }
}