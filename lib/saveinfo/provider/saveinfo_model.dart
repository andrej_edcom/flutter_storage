import 'package:ariadna/saveinfo/repo/saveinfo_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/provider/base_model.dart';
import 'package:ariadna/base/provider/viewstate.dart';
import 'package:ariadna/base/api/api_response.dart';

class SaveinfoModel extends BaseModel {
  SaveinfoRepository fininfoRepository = locator<SaveinfoRepository>();

  ApiResponse apiResponseFininfos = ApiResponse.loading('Fetching Fininfo');

  Future getApiResponseFininfos(String name) async {
    debugPrint('Andrejko data: $name');

    if( name.length > 3 ) {
      apiResponseFininfos = ApiResponse.loading("");
      setState(ViewState.Busy);
      try {
        List<Fininfo> fins = await fininfoRepository.fetchDigiSkFininfoList(name);
        apiResponseFininfos = ApiResponse.completed(fins);
      } catch (e) {
        apiResponseFininfos = ApiResponse.error(e.toString());
        print(e);
      }
      setState(ViewState.Idle);
    }

  }

  Future initFininfo() async {
    List<Fininfo> _fininfos = [];
    apiResponseFininfos = ApiResponse.completed(_fininfos);
  }

  Future<bool> saveInfo(Fininfo info) async {
    String jwt = await getJwt();
    String _deviceId = await deviceId();
    String xfir = await fininfoRepository.getFir(_deviceId, 'user.fir');
    print("Andrejko saveinfoModel jwt " + jwt);
    return await fininfoRepository.saveFir(jwt, info, xfir);
  }

  Future<String> deviceId() async {
    return fininfoRepository.getDeviceId();
  }

  Future<String> getJwt() async {
    String _deviceId = await deviceId();
    String jwt = await fininfoRepository.getJwt(_deviceId, 'user.jwt');
    if(jwt == null) { jwt = ""; }
    return jwt;
  }

}