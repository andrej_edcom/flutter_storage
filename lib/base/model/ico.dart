import 'package:equatable/equatable.dart';
import 'package:validators/validators.dart' as validator;

class Ico extends Equatable {
  String ico;
  String dic;
  String icd;
  String nai;
  String uli;
  String mes;
  String psc;
  String tel;
  String email;
  String ib1;
  String sw1;

  Ico({
    this.ico,
    this.dic,
    this.icd,
    this.nai,
    this.uli,
    this.mes,
    this.psc,
    this.tel,
    this.email,
    this.ib1,
    this.sw1,
  });

  @override
  List<Object> get props => [ico, dic, icd, nai, uli, mes, psc, tel, email, ib1, sw1];

  factory Ico.fromJson(Map<String, dynamic> json) {
    return Ico(ico: json['ico'], dic: json['dic'], icd: json['icd'], nai: json['nai'], uli: json['uli']
        , mes: json['mes'], psc: json['psc'], tel: json['tel'], email: json['email'], ib1: json['ib1'], sw1: json['sw1']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ico'] = this.ico;
    data['dic'] = this.dic;
    data['icd'] = this.icd;
    data['nai'] = this.nai;
    data['uli'] = this.uli;
    data['mes'] = this.mes;
    data['psc'] = this.psc;
    data['tel'] = this.tel;
    data['email'] = this.email;
    data['ib1'] = this.ib1;
    data['sw1'] = this.sw1;
    return data;
  }

  bool validDic(String value){
    return (validator.isNumeric(value) || value == "");
  }

  String wrongDic(){
    return "form_wrong_dic";
  }

  bool validIcd(String value){
    return (validator.isAlphanumeric(value) || value == "");
  }

  String wrongIcd(){
    return "form_wrong_icd";
  }

  bool validNai(String value) {

    final validCharacters = RegExp(r'^[a-zA-Z0-9 _&\-=@,\.;]+$');
    return validCharacters.hasMatch(value);
  }

  String wrongNai(){
    return "form_wrong_nai";
  }

  bool validUli(String value){
    final validCharacters = RegExp(r'^[a-zA-Z0-9ľĺščťžýáíéěřäúôňüöĽĹŠČŤŽÝÁÉĚŘÄÚÔŇÜŐ _&\-=@,\.;\/]+$');
    return validCharacters.hasMatch(value);
  }

  String wrongUli(){
    return "form_wrong_uli";
  }

  bool validPsc(String value){
    return (validator.isNumeric(value) || value == "");
  }

  String wrongPsc(){
    return "form_wrong_psc";
  }

  bool validMes(String value){
    final validCharacters = RegExp(r'^[a-zA-Z0-9ľĺščťžýáíéěřäúôňüöĽĹŠČŤŽÝÁÉĚŘÄÚÔŇÜŐ _&\-=@,\.;\/]+$');
    return validCharacters.hasMatch(value);
  }

  String wrongMes(){
    return "form_wrong_mes";
  }

  bool validTel(String value){
    final validCharacters = RegExp(r'^[0-9+\/]+$');
    return ( validCharacters.hasMatch(value) || value == "");
  }

  String wrongTel(){
    return "form_wrong_tel";
  }

  bool validEmail(String value){
    return (validator.isEmail(value) || value == "");
  }

  String wrongEmail(){
    return "form_wrong_email";
  }

  bool validIb1(String value){
    return (validator.isAlphanumeric(value) || value == "");
  }

  String wrongIb1(){
    return "form_wrong_ib1";
  }

  bool validSw1(String value){
    return (validator.isAlphanumeric(value) || value == "");
  }

  String wrongSw1(){
    return "form_wrong_sw1";
  }

}
