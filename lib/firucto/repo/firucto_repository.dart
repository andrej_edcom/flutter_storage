import 'package:ariadna/base/api/api_base_helper.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/localStoragePlugin/local_storage_provider.dart';

class FiructoRepository {

  ApiBaseHelper helper = locator<ApiBaseHelper>();
  AppSharedPreferences prefs = locator<AppSharedPreferences>();

  Future<String> getDeviceId() async {
    return prefs.getDeviceId();
  }

  void saveFir(String userId, String key, String value) async {
    String _deviceId = await getDeviceId();
    await prefs.saveString(_deviceId, key, value);
  }

  Future<String> getFir(String userId, String key) async {
    String _deviceId = await getDeviceId();
    return prefs.getString(_deviceId, key);
    //return "30 - Firma 30 2020";
  }

}
