import 'package:ariadna/post/repo/post.dart';

class PostResponse {
  List<Post> results;

  PostResponse({this.results});

  PostResponse.fromJson(List<dynamic> json) {

      results = new List<Post>();
      results = json.map((i)=>Post.fromJson(i)).toList();

  }

}