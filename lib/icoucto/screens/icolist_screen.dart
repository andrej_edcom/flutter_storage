import 'package:ariadna/base/ui/widgets/navigationDrawer/navigation_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ariadna/icoucto/bloc/icolist_bloc.dart';
import 'package:ariadna/base/model/ico.dart';
import 'package:ariadna/icoucto/data/icolist_repository.dart';
import 'package:ariadna/base/ui/widgets/PillShapedButton.dart';
import 'package:ariadna/app_localizations.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/firucto/bloc/firucto_bloc.dart';
import 'package:ariadna/icoucto/widgets/ico_list_item.dart';

class IcolistScreen extends StatefulWidget {
  @override
  _IcolistScreenState createState() => _IcolistScreenState();
}

class _IcolistScreenState extends State<IcolistScreen> {

  //IcoListBloc icoListBloc = IcoListBloc(FakeIcolistRepository());
  IcoListBloc icoListBloc = locator<IcoListBloc>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => icoListBloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).translate('account_companies') + " " + " ",),
        ),
        drawer: navigationDrawer(),
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 16),
          alignment: Alignment.center,
          child: SingleChildScrollView(
              physics: ScrollPhysics(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CityInputField(),
                  BlocConsumer<IcoListBloc, IcoListState>(
                    listener: (context, state) {
                      if (state is IcoListError) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text(AppLocalizations.of(context).translate(state.message) + " "),
                          ),
                        );
                      }
                    },
                    builder: (context, state) {
                      if (state is IcoListInitial) {
                        return buildInitialInput();
                      } else if (state is IcoListLoading) {
                        return buildLoading();
                      } else if (state is IcoListLoaded) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            getIcosUi(state.listIco, state.cityName),
                          ],
                        );
                      } else {
                        // (state is IcoError)
                        return buildInitialInput();
                      }
                    },
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget getIcosUi(List<Ico> icos, String cityName) => ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: icos.length,
      itemBuilder: (context, index) => IcoListItem(
            icoItem: icos[index],
            onTap: () {
              Navigator.of(context)
                  .pushNamed('/icoedit', arguments: {'item': icos[index], 'cityName': cityName});
            },
          ));

  Widget buildInitialInput() {
    return Center(
      child: Container(),
    );
  }

  Widget buildLoading() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 20,),
        Container(
          child: CircularProgressIndicator(),
        ),
      ],
    );
  }

}

class CityInputField extends StatelessWidget {
  FiructoBloc bloc = locator<FiructoBloc>();
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: [
          StreamBuilder<String>(
            stream: bloc.fiructoStream,
            builder: (context, snapshotFir) {
              return AnimatedContainer(
                height: 40,
                width: 280,
                duration: Duration(milliseconds: 100),
                curve: Curves.fastOutSlowIn,
                child: Hero(
                  tag: "heroFir",
                  child: PillShapedButton(
                    title: ( snapshotFir.data.toString() == "" || snapshotFir.data == null )
                        ? AppLocalizations.of(context)
                                .translate('account_company') +
                            " "
                        : AppLocalizations.of(context).translate('company') +
                            " " +
                            snapshotFir.data.toString() +
                            " ",
                    onPressed: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      Navigator.pushNamed(context, '/firucto').then((value) {});
                    },
                  ),
                ),
              );
            },
          ),
          SizedBox(
            height: 15,
          ),
          TextField(
            onSubmitted: (value) => submitCityName(context, value),
            textInputAction: TextInputAction.search,
            controller: controller,
            decoration: InputDecoration(
              hintText: AppLocalizations.of(context).translate('searched_text') + " ",
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
              suffixIcon: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    submitCityName(context, controller.text);
                  }),
            ),
          ),
        ],
      ),
    );
  }

  void submitCityName(BuildContext context, String cityName) {
    FocusScope.of(context).requestFocus(new FocusNode());
    final icoListBloc = context.read<IcoListBloc>();
    icoListBloc.add(GetIcoList(cityName));
  }
}
