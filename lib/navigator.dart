import 'package:ariadna/base/model/ico.dart';
import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'package:ariadna/fininfo/ui/screens/fininfo_screen.dart';
import 'package:ariadna/firucto/ui/screens/firucto_screen.dart';
import 'package:ariadna/icoucto/screens/icoedit_screen.dart';
import 'package:ariadna/icoucto/screens/icolist_screen.dart';
import 'package:ariadna/saveinfo/ui/screens/saveinfo_screen.dart';
import 'package:ariadna/weather/pages/weather_search_blocpage.dart';
import 'package:flutter/material.dart';
import 'package:ariadna/movie/ui/views/movie_screen.dart';
import 'package:ariadna/login/ui/screens/login_screen.dart';
import 'package:ariadna/post/ui/screens/posts_screen.dart';

PageRouteBuilder buildPageRouteBuilder(
    BuildContext context, RouteSettings settings) {
  return PageRouteBuilder(
    transitionDuration: Duration(seconds: 1),
    pageBuilder: (_, __, ___) => _makeRoute(
        context: context,
        routeName: settings.name,
        arguments: settings.arguments),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

Widget _makeRoute(
    {@required BuildContext context,
    @required String routeName,
    Object arguments}) {
  final Widget child = _buildRoute(
    context: context,
    routeName: routeName,
    arguments: arguments,
  );
  return child;
}

Widget _buildRoute({
  @required BuildContext context,
  @required String routeName,
  Object arguments,
}) {
  switch (routeName) {
    case '/login':
      return LoginScreen();

    case '/firucto':
      return FiructoScreen();

    case '/saveinfo':
      final map = arguments as Map<String, dynamic> ?? Map();
      final item = map['item'] as Fininfo;
      return SaveinfoScreen(item: item);

    case '/icolist':
      return IcolistScreen();

    case '/icoedit':
      final map = arguments as Map<String, dynamic> ?? Map();
      final item = map['item'] as Ico;
      final cityName = map['cityName'] as String;
      return IcoEditScreen(item: item, cityName: cityName);

    case '/fininfo':
      return FinInfoScreen();

    case '/movie':
      return MovieScreen();

    case '/posts':
      return PostsScreen();

    case '/logout':

    default:
      return Container();
  }
}
