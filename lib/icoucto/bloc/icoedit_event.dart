part of 'icoedit_bloc.dart';

@immutable
abstract class IcoEditEvent {}

class SaveIco extends IcoEditEvent {
  final Ico updateIco;

  SaveIco(this.updateIco);
}