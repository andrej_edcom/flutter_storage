import 'dart:async';
import 'package:ariadna/locator.dart';
import 'package:ariadna/login/repo/user.dart';
import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/base/bloc/bloc.dart';
import 'package:ariadna/login/repo/user_repository.dart';

class UserBloc implements Bloc {
  User _user;
  UserRepository repository = locator<UserRepository>();

  User get selectedUser => _user;

  final _userController = StreamController<ApiResponse<List<User>>>();

  Stream<ApiResponse<List<User>>> get userStream => _userController.stream;

  UserBloc() {
    getUser();
  }

  void selectUser(User user) {
    _user = user;
    //_userController.sink.add(user);
  }

  Future<String> deviceId() async {
    return repository.getDeviceId();
  }

  void saveUser(User user) async {

    String _deviceId = await deviceId();
    await repository.saveString(_deviceId, 'user.name', user.name);
    await repository.saveString(_deviceId, 'user.id', user.id);
    await repository.saveString(_deviceId, 'user.jwt', user.jwt);

    _user = user;
    //_userController.sink.add(user);
  }

  void getUser() async {

    //print("Andrejko UserBloc ");
    _userController.sink.add(ApiResponse.loading('Fetching loggedIn User'));

    try {

      String _deviceId = await deviceId();
      final name = await repository.getString(_deviceId, 'user.name');
      final id = await repository.getString(_deviceId, 'user.id');
      final jwt = await repository.getString(_deviceId, 'user.jwt');

      final user = User(name: name, id: id, jwt: jwt);
      _user = user;

      var listUser = new List<User>();
      listUser.add(user);
      _userController.sink.add(ApiResponse.completed(listUser));

    } catch (e) {
      _userController.sink.add(ApiResponse.error(e.toString()));
      print(e);
    }

  }

  void getNextUser() async {
    print("Andrejko UserBloc next ");
    _userController.sink.add(ApiResponse.loading('Fetching loggedIn User'));
  }

  void loginUser(String username, String pswd) async {

    String _deviceId = await deviceId();
    String jwt = "";

    try {

      jwt = await repository.attemptLogin(username, pswd);
      await repository.saveString(_deviceId, 'user.jwt', jwt);

      if(jwt != "") {
        final user = User(name: "name 1", id: "1", jwt: jwt);
        var listUser = new List<User>();
        listUser.add(user);

        _user = user;
        _userController.sink.add(ApiResponse.completed(listUser));
      } else {
        _userController.sink.add(ApiResponse.error("nomatch_userpswd"));
      }

    } catch (e) {
      print("Andrejko UserBloc jwt catch e");
      _userController.sink.add(ApiResponse.error(e.toString()));
      print(e);
    }



  }

  void logoutUser() async {

    String _deviceId = await deviceId();
    await repository.saveString(_deviceId, 'user.jwt', "");

    final user = User(name: "", id: "", jwt: "");
    var listUser = new List<User>();
    listUser.add(user);

    _user = user;
    _userController.sink.add(ApiResponse.completed(listUser));

  }

  @override
  void dispose() {
    _userController.close();
  }
}
