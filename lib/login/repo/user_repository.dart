import 'package:ariadna/base/api/api_base_helper.dart';
import 'package:ariadna/login/repo/user.dart';
import 'user_response.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/login/util/constants.dart';
import 'package:ariadna/base/localStoragePlugin/local_storage_provider.dart';

class UserRepository {

  ApiBaseHelper helper = locator<ApiBaseHelper>();
  AppSharedPreferences prefs = locator<AppSharedPreferences>();

  Future<List<User>> fetchMovieList() async {
    final response = await helper.get(Constants.LOGIN_DOMAIN, "movie/popular?api_key=" + Constants.LOGIN_API_KEY);
    return UserResponse.fromJson(response).results;
  }


  Future<String> getDeviceId() async {
    return prefs.getDeviceId();
  }

  void saveString(String userId, String key, String value) async {
    String _deviceId = await getDeviceId();
    await prefs.saveString(_deviceId, key, value);
  }

  Future<String> getString(String userId, String key) async {
    String _deviceId = await getDeviceId();
    return prefs.getString(_deviceId, key);
  }

  Future<String> attemptLogin(String username, String pswd) async {
    return await helper.attemptLogIn(Constants.LOGIN_URL, username, pswd);
  }

}