import 'package:flutter/material.dart';
import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/base/bloc/bloc_provider.dart';
import 'package:ariadna/base/ui/widgets/error_dialog.dart';
import 'package:ariadna/movie/bloc/movie_bloc.dart';
import 'package:ariadna/movie/repo/movie_response.dart';
import 'package:ariadna/movie/ui/widgets/movie_list.dart';
import 'package:ariadna/movie/ui/widgets/loading_progress_bar.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/login/bloc/user_bloc.dart';

/**
 * Handling Network Calls like a Pro in Flutter
 * by https://medium.com/flutter-community/handling-network-calls-like-a-pro-in-flutter-31bd30c86be1
 * code https://github.com/xsahil03x/flutter_network_handling
 */
class MovieScreen extends StatefulWidget {
  @override
  _MovieScreenState createState() => _MovieScreenState();
}

class _MovieScreenState extends State<MovieScreen> {
  MovieBloc bloc;

  @override
  void initState() {
    super.initState();
    //without DI locator GetIt
    //_bloc = MovieBloc();

    //with DI locator GetIt
    bloc = locator<MovieBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MovieBloc>(
      bloc: bloc,
      child: Scaffold(
        appBar:
        AppBar(elevation: 0.0, title: Text('Ariadna'), actions: <Widget>[
          FlatButton(
              child: Icon(Icons.chat),
              onPressed: () {
                Navigator.pushNamed(context, '/posts');
              }),
          FlatButton(
              child: Icon(Icons.logout),
              onPressed: () {
                final userBloc = BlocProvider.of<UserBloc>(context);
                userBloc.logoutUser();
                //Navigator.pushNamedAndRemoveUntil(context, '/login', (_) => false);
              }),
        ]),
        body: RefreshIndicator(
          onRefresh: () => bloc.fetchMovieList(),
          child: StreamBuilder<ApiResponse<List<Movie>>>(
            stream: bloc.movieListStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                switch (snapshot.data.status) {
                  case Status.LOADING:
                    return LoadingProgressBar(loadingMessage: snapshot.data.message);
                    break;
                  case Status.COMPLETED:
                    return MovieList(movieList: snapshot.data.data);
                    break;
                  case Status.ERROR:
                    return ErrorDialog(
                      errorMessage: snapshot.data.message,
                      onRetryPressed: () => bloc.fetchMovieList(),
                    );
                    break;
                }
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

}