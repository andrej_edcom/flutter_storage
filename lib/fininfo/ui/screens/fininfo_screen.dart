import 'package:ariadna/base/ui/widgets/navigationDrawer/navigation_drawer.dart';
import 'package:ariadna/firucto/bloc/firucto_bloc.dart';
import 'package:flutter/material.dart';
import 'package:ariadna/app_localizations.dart';
import 'package:ariadna/base/ui/widgets/PillShapedButton.dart';
import 'package:ariadna/base/provider/base_view_provider.dart';
import 'package:ariadna/base/bloc/bloc_provider.dart';
import 'package:ariadna/login/bloc/user_bloc.dart';
import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'package:ariadna/base/api/api_response.dart';
import 'package:ariadna/fininfo/provider/fininfo_model.dart';
import 'package:ariadna/base/ui/widgets/error_dialog.dart';
import 'package:ariadna/fininfo/ui/widgets/fininfo_list_item.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/base/ui/widgets/alert_dialog.dart';


class FinInfoScreen extends StatefulWidget {
  @override
  _FinInfoScreenState createState() => _FinInfoScreenState();
}

class _FinInfoScreenState extends State<FinInfoScreen> {
  FiructoBloc bloc;
  final TextEditingController _controller = TextEditingController();

  double _height = 50;
  double _width = 150;
  double _heightFir = 40;
  double _widthFir = 280;

  @override
  void initState() {
    super.initState();

    bloc = locator<FiructoBloc>();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(elevation: 0.0, title: Text('Ariadna'), actions: <Widget>[
          FlatButton(
              child: Icon(Icons.logout),
              onPressed: () {
                final userBloc = BlocProvider.of<UserBloc>(context);
                userBloc.logoutUser();
                final fiructoBloc = BlocProvider.of<FiructoBloc>(context);
                fiructoBloc.removeFiructo();
              }),
        ]),
        drawer: navigationDrawer(),
        body: StreamBuilder<String>(
          stream: bloc.fiructoStream,
          builder: (context, snapshotFir) {
            return screenWithApiResponse(context, snapshotFir);
          },
        ));
  }

  Widget screenWithApiResponse(BuildContext context, snapshotFir) =>
      BaseViewProvider<FininfoModel>(
        onModelReady: (model) => model.initFininfo(),
        builder: (context, model, child) => Scaffold(
          body: Container(
            height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                    Color(0xA6F6F1),
                    Color(0x46BF58),
                    Color(0xFF035762)
                  ],
                      stops: [
                    0,
                    0.35,
                    1.0
                  ])),
              child: SingleChildScrollView(child: bodyWithApiResponse(model, snapshotFir))),
        ),
      );

  Widget bodyWithApiResponse(FininfoModel model, snapshotFir) {
    switch (model.apiResponseFininfos.status) {
      case Status.LOADING:
        return Center(child: CircularProgressIndicator());
        break;
      case Status.COMPLETED:
        return SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            children: <Widget>[
            //_buildFinBody(context, model),
            findCompanyWidget(model, snapshotFir),
            getCompaniesUi(model.apiResponseFininfos.data),
            ],
          ),
        );
        break;
      case Status.ERROR:
        return ErrorDialog(
          errorMessage: model.apiResponseFininfos.message,
          onRetryPressed: () => model.initFininfo(),
        );
        break;
    }
  }

  SingleChildScrollView findCompanyWidget(FininfoModel model, snapshotFir) {
    return SingleChildScrollView(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(height: 10),
            Container(
              width: 180,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(context).translate('finstat_info') +
                        " ",
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xff000000),
                    ),
                  ),
                ],
              ),
            ),
            Container(height: 10),
            AnimatedContainer(
              height: _heightFir,
              width: _widthFir,
              duration: Duration(milliseconds: 100),
              curve: Curves.fastOutSlowIn,
              child: Hero(
                tag: "heroFir",
                child: PillShapedButton(
                  title: ( snapshotFir.data.toString() == "" || snapshotFir.data == null )
                      ? AppLocalizations.of(context)
                              .translate('account_company') +
                          " "
                      : AppLocalizations.of(context).translate('company') +
                          " " +
                          snapshotFir.data.toString() +
                          " ",
                  onPressed: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    if (!mounted) return;
                    setState(() {
                      _heightFir = 40;
                      _widthFir = 300;
                    });
                    Navigator.pushNamed(context, '/firucto').then((value) {
                      setState(() {
                        _heightFir = 40;
                        _widthFir = 280;
                      });
                    });
                  },
                ),
              ),
            ),
            Container(height: 10),
            Container(
              width: 185,
              child: TextField(
                controller: _controller,
                autofocus: true,
                decoration: InputDecoration(
                  labelText:
                      AppLocalizations.of(context).translate('label_company'),
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                    fontSize: 16,
                    color: Color(0xff9b9b9b),
                  ),
                  hintText:
                      AppLocalizations.of(context).translate('hint_company'),
                  hintMaxLines: 1,
                ),
              ),
            ),
            Container(height: 5),
            AnimatedContainer(
              height: _height,
              width: _width,
              duration: Duration(milliseconds: 100),
              curve: Curves.fastOutSlowIn,
              child: Hero(
                tag: "heroChoose",
                child: PillShapedButton(
                  title:
                      AppLocalizations.of(context).translate('find_info') + " ",
                  onPressed: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    if (snapshotFir.data.toString() != "") {
                      if (!mounted) return;
                      setState(() {
                        _height = 50;
                        _width = 200;
                      });
                      model.getApiResponseFininfos(_controller.text);
                      //model.demoFininfo();
                      setState(() {
                        _height = 50;
                        _width = 150;
                      });
                    } else {
                      displayDialog(
                          context,
                          AppLocalizations.of(context).translate('error_occur'),
                          AppLocalizations.of(context)
                              .translate('choose_firucto'));
                    }
                  },
                ),
              ),
            ),
            Container(height: 10),
          ]),
    );
  }

  Widget getCompaniesUi(List<Fininfo> fins) => ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: fins.length,
      itemBuilder: (context, index) => FininfoListItem(
        fininfo: fins[index],
        onTap: () {
          Navigator.of(context)
              .pushNamed('/saveinfo', arguments: {'item': fins[index]});
        },

      )
  );
}
