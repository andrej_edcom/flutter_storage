import 'package:ariadna/base/api/api_base_helper.dart';
import 'movie_response.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/movie/util/constants.dart';

class MovieRepository {

  //without DI locator GetIt
  //ApiBaseHelper _helper = ApiBaseHelper();

  //with DI locator GetIt
  ApiBaseHelper helper = locator<ApiBaseHelper>();

  Future<List<Movie>> fetchMovieList() async {
    final response = await helper.get(Constants.MOVIE_DOMAIN, "movie/popular?api_key=" + Constants.MOVIE_API_KEY);
    return MovieResponse.fromJson(response).results;
  }
}
