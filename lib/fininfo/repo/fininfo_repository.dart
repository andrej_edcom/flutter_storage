import 'package:ariadna/base/api/api_base_helper.dart';
import 'package:ariadna/fininfo/repo/fininfo.dart';
import 'fininfo_response.dart';
import 'package:ariadna/locator.dart';
import 'package:ariadna/fininfo/util/constants.dart';

class FininfoRepository {

  ApiBaseHelper helper = locator<ApiBaseHelper>();

  Future<List<Fininfo>> fetchFininfoList(int userId) async {
    final response = await helper.get(Constants.FININFO_DOMAIN, "posts?userId=$userId");
    return FininfoResponse.fromJson(response).results;
  }

  Future<List<Fininfo>> fetchDigiSkFininfoList(String name) async {
    final response = await helper.get(Constants.EKOSYSDIG_DOMAIN, "corporate_bodies/search?q=name:$name" + "&private_access_token=" + Constants.EKOSYSDIG_API_KEY);
    return FininfoResponse.fromJson(response).results;
  }

  Future<List<Fininfo>> fetchDigiSkIcoFininfoList(String name) async {
    final response = await helper.get(Constants.EKOSYSDIG_DOMAIN, "corporate_bodies/search?q=cin:$name" + "&private_access_token=" + Constants.EKOSYSDIG_API_KEY);
    return FininfoResponse.fromJson(response).results;
  }

}
