import 'package:flutter/material.dart';

void displayDialog(context, title, text) => showDialog(
  context: context,
  barrierDismissible: false, // user must tap button!
  builder: (context) =>
      AlertDialog(
        title: Text(title),
        content: Text(text),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))
        ),
        actions: <Widget>[
          FlatButton(
              child: Text('Ok '),
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ],
      ),
);