import 'dart:typed_data';
import 'local_storage.dart';

/*
LocalKeyValuePersistence to SharedPreferences
 */
class WebSharedPreferences implements LocalStorage {

  @override
  Future<String> saveImage(String userId, String key, Uint8List image) async {

    return null;
  }

  @override
  void saveObject(
      String userId, String key, Map<String, dynamic> object) async {

  }

  @override
  void saveString(String userId, String key, String value, [String jwtid]) async {

  }

  @override
  Future<Uint8List> getImage(String userId, String key) async {

    return null;
  }

  @override
  Future<Map<String, dynamic>> getObject(String userId, String key) async {

    return null;
  }

  @override
  Future<String> getString(String userId, String key, [String jwtid]) async {

    return null;
  }

  @override
  Future<void> removeImage(String userId, String key) async {

  }

  @override
  Future<void> removeObject(String userId, String key) async {

  }

  @override
  Future<void> removeString(String userId, String key, [String jwtid]) async {

  }

}
